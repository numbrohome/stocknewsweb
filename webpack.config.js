var path = require('path');
var webpack = require('webpack');

var debug = process.env.NODE_ENV !== 'production';

module.exports = {
    devtool: 'cheap-module-eval-source-map',
    entry: [
        './app/src/main.js'
    ],
    output: {
        path: './app/dist/',
        filename: 'bundle.js',
        publicPath: '/',
        hot: true
    },
    devServer: {
        inline: true,
        contentBase: './app/dist'
    },
    plugins: debug ? [] : [
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false })
    ],
    externals: {
        'Config': JSON.stringify(debug ? {
            // Development API
            serverUrl: "http://api.stocknews.local/"
        } : {
            // Production API
            serverUrl: "http://api.stocknews.local/"
        })
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel',
                query: {
                    presets: ['es2015', 'react']
                }
            },
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"
            },
            {
                test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff$|\.ttf$|\.eot$|\.woff2$|\.wav$|\.mp3$/,
                loader: "file-loader"
            },
            {
                test: /\.scss$|\.sass$/,
                loaders: ['style', 'css', 'sass']
            }
        ]
    }
};