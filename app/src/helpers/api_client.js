import axios from 'axios';
import Config from 'Config';

export default class ApiClient {
    static getCompanies() {
        return axios.get(`${Config.serverUrl}api/companies`);
    }

    static getCompany(tickerCode) {
        return axios.get(`${Config.serverUrl}api/companies/${tickerCode}`);
    }
}