import React from "react"
import ReactDOM from "react-dom"
import Home from "./home/home.jsx"
import { Router, Route, IndexRoute, hashHistory } from "react-router"

export default () => {
    return (
        <Router history={ hashHistory }>
            <Route path="/" component={ Home }>
            </Route>
        </Router>
    );
}