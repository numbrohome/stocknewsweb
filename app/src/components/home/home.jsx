import React from 'react';
import StockSidebar from '../stock_sidebar/stock_sidebar.jsx';
import StockOverview from '../stock_overview/stock_overview.jsx';
import { Grid, Row, Col } from 'react-bootstrap';
import ApiClient from '../../helpers/api_client';
import './home.sass';

export default class Home extends React.Component {
    constructor() {
        super();
        this.state = {
            companies: [],
            company: null
        }
    }

    handleGetCompany(tickerCode) {
        ApiClient.getCompany(tickerCode).then((response) => {
            this.setState({
                company: response.data
            });
        });
    }

    componentDidMount() {
        ApiClient.getCompanies().then((response) => {
            this.setState({
                companies: response.data
            });
        });
    }

    render() {
        return (
            <Grid id="home-component">
                <Row>
                    <Col id="nav-column" md={4}>
                        <Row>
                            <Col md={12}>
                                <h1>Stock News</h1>
                            </Col>
                        </Row>
                        <StockSidebar getCompany={this.handleGetCompany.bind(this)} companies={this.state.companies} />
                    </Col>
                    <Col md={8}>
                        <StockOverview company={this.state.company} />
                    </Col>
                </Row>
                <Row>
                    <Col md={6}></Col>
                    <Col md={6}></Col>
                </Row>
            </Grid>
        );
    }
}