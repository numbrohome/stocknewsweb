import expect from 'expect';
import React from 'react';
import TestUtils from 'react-addons-test-utils';
import expectJSX from 'expect-jsx';
import StockList from '../stock_list/stock_list.jsx';
import StockSidebar from './stock_sidebar.jsx'

expect.extend(expectJSX);

describe('StockSidebar', () => {
    it('has a StockList component', () => {
        const getCompany = function(){};
        const renderer = TestUtils.createRenderer();
        renderer.render(<StockSidebar companies={[]} getCompany={getCompany} />);

        const actual = renderer.getRenderOutput();
        const expected = (
            <StockList companies={[]} getCompany={getCompany} />
        );

        expect(actual).toIncludeJSX(expected);
    });
});