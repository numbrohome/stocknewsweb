import React from 'react';
import StockList from '../stock_list/stock_list.jsx';
import { Row, Col } from 'react-bootstrap'
import './stock_sidebar.sass'

class StockSidebar extends React.Component {
    render() {
        return (
            <Row>
                <Col md={12}>
                    <StockList getCompany={this.props.getCompany} companies={this.props.companies} />
                </Col>
            </Row>
        );
    }
}

StockSidebar.propTypes = {
    companies: React.PropTypes.array.isRequired,
    getCompany: React.PropTypes.func.isRequired
};

StockSidebar.defaultProps = {
    companies: []
};

export default StockSidebar;