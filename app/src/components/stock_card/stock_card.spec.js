import expect from 'expect';
import React from 'react';
import TestUtils from 'react-addons-test-utils';
import expectJSX from 'expect-jsx';
import StockCard from './stock_card.jsx';
import { Glyphicon } from 'react-bootstrap';

expect.extend(expectJSX);

describe('StockCard', () => {
    let company = {
        "name": "Google Inc",
        "tickerCode": "GOOG",
        "latestPrice": 54407,
        "priceUnits": "GBP:pence",
        "asOf": "2016-03-28T21:07:32.444Z",
        "storyFeedUrl": "http://mm-recruitment-story-feed-api.herokuapp.com/8271",
        "storyFeed": [
            {
                "id": 74,
                "headline": "Google going strong, but maybe not for long.",
                "body": "Google has some concerns to address the balance of this year, and beyond. Over the long run, the consensus analyst recommendation for Google as a 'strong buy' is warranted as the company continues driving a healthy double-digit top line growth. But that doesn't mean there won't be a hurdle, or three, to overcome along the way.",
                "positivity": "neutral"
            },
            {
                "id": 141,
                "headline": "Ad revenues still primary source of Google revenue.",
                "body": "Investors were encouraged by a healthy gain in the number of people looking at Google's ads, even as the average prices for those marketing messages extended a three-and-half year slump. The market also had been bracing for more disappointing numbers, triggering a 'relief rally' when the results weren't as bad as feared, BGC Partners analyst Colin Gillis said.",
                "positivity": "negative"
            }
        ]
    };

    const renderStockCard = function(company) {
        const renderer = TestUtils.createRenderer();
        renderer.render(<StockCard company={company} />);

        return renderer;
    };

    it('has a header with company name', () => {
        const renderer = renderStockCard(company);
        const actual = renderer.getRenderOutput();
        const expected = (
            <h1>{company.name}</h1>
        );

        expect(actual).toIncludeJSX(expected);
    });

    it('has an icon with neutral feedback', () => {
        const renderer = renderStockCard(company);
        const actual = renderer.getRenderOutput();
        const expected = (
            <Glyphicon glyph={'info-sign'} />
        );

        expect(actual).toIncludeJSX(expected);
    });

    it('has an icon with negative feedback', () => {
        const renderer = renderStockCard(company);
        const actual = renderer.getRenderOutput();
        const expected = (
            <Glyphicon glyph={'thumbs-down'} />
        );

        expect(actual).toIncludeJSX(expected);
    });

    it('has an information about stock price', () => {
        const renderer = renderStockCard(company);
        const actual = renderer.getRenderOutput();
        const expected = '54407 GBP';

        expect(actual).toIncludeJSX(expected);
    });

    it('has a right ticker code', () => {
        const renderer = renderStockCard(company);
        const actual = renderer.getRenderOutput();
        const expected = 'GOOG';

        expect(actual).toIncludeJSX(expected);
    });
});