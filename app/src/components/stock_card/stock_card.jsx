import React from 'react';
import { Row, Col, Glyphicon } from 'react-bootstrap';
import './stock_card.sass';

export default class StockCard extends React.Component {
    formatPrice(price, units) {
        return `${price} ${units.split(':')[0]}`;
    }

    render() {
        let company = this.props.company;

        return (
            <div id="stock-card">
                <Row>
                    <Col md={12}><h1>{company.name}</h1></Col>
                </Row>
                <Row className="stock-card-info-block">
                    <Col className="stock-card-ticker-code" xs={2}>{company.tickerCode}</Col>
                    {(() => {
                        if (company.hasOwnProperty('latestPrice')) {
                            return <Col className="stock-card-latest-price" xs={10}>{this.formatPrice(company.latestPrice, company.priceUnits)}</Col>
                        }
                    })()}
                </Row>
                <Row>
                    <Col md={12}>
                        {(() => {
                            if (company.hasOwnProperty('storyFeed')) {
                                return company.storyFeed.map((story) => {
                                    return (
                                        <Row className="stock-card-story" key={story.id}>
                                            <Col sm={11}>
                                                <h2>{story.headline}</h2>
                                                <div className="stock-card-text">
                                                    {story.body}
                                                </div>
                                            </Col>
                                            <Col sm={1}>
                                                <Glyphicon glyph={{
                                                'neutral': 'info-sign',
                                                'negative': 'thumbs-down',
                                                'positive': 'thumbs-up'
                                                }[story.positivity]} />
                                            </Col>
                                        </Row>
                                    )
                                });
                            }
                        })()}
                    </Col>
                </Row>
            </div>
        );
    }
}