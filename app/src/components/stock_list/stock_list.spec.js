import expect from 'expect';
import React from 'react';
import TestUtils from 'react-addons-test-utils';
import expectJSX from 'expect-jsx';
import StockList from './stock_list.jsx';
import { Nav, NavItem } from 'react-bootstrap';

expect.extend(expectJSX);

describe('StockList', () => {
    let companies = [
        {
            "name": "Microsoft Inc",
            "tickerCode": "MSFT"
        },
        {
            "name": "Google Inc",
            "tickerCode": "GOOG"
        },
        {
            "name": "Apple Inc",
            "tickerCode": "AAPL"
        }
    ];

    it('has a list of companies', () => {
        const getCompany = function(){};
        const renderer = TestUtils.createRenderer();
        renderer.render(<StockList getCompany={getCompany} companies={companies} />);

        const actual = renderer.getRenderOutput();
        const expected = (
            <Nav id="stock-list-component" bsStyle="pills" stacked onSelect={getCompany}>
                <NavItem key={'MSFT'} eventKey={'MSFT'}>Microsoft Inc</NavItem>
                <NavItem key={'GOOG'} eventKey={'GOOG'}>Google Inc</NavItem>
                <NavItem key={'AAPL'} eventKey={'AAPL'}>Apple Inc</NavItem>
            </Nav>
        );

        expect(actual).toIncludeJSX(expected);
    });
});