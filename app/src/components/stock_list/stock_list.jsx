import React from 'react';
import { Nav, NavItem } from 'react-bootstrap';
import './stock_list.sass';

class StockList extends React.Component {
    render() {
        return (
            <Nav id="stock-list-component" bsStyle="pills" stacked onSelect={this.props.getCompany}>
                {
                    this.props.companies.map((company, i) => {
                        return (
                            <NavItem key={company.tickerCode} eventKey={company.tickerCode}>{company.name}</NavItem>
                        );
                    })
                }
            </Nav>
        );
    }
}


StockList.propTypes = {
    companies: React.PropTypes.array.isRequired,
    getCompany: React.PropTypes.func.isRequired
};

StockList.defaultProps = {
    companies: []
};


export default StockList;