import expect from 'expect';
import React from 'react';
import TestUtils from 'react-addons-test-utils';
import expectJSX from 'expect-jsx';
import StockCard from '../stock_card/stock_card.jsx';
import StockOverview from './stock_overview.jsx'

expect.extend(expectJSX);

describe('StockOverview', () => {
    it('has a StockCard component when company exists', () => {
        const renderer = TestUtils.createRenderer();
        renderer.render(<StockOverview company={{}} />);

        const actual = renderer.getRenderOutput();
        const expected = (
            <StockCard company={{}} />
        );

        expect(actual).toIncludeJSX(expected);
    });

    it('does not have a StockCard component without company', () => {
        const renderer = TestUtils.createRenderer();
        renderer.render(<StockOverview company={null} />);

        const actual = renderer.getRenderOutput();
        const expected = (
            <StockCard company={null} />
        );

        expect(actual).toNotIncludeJSX(expected);
    });
});