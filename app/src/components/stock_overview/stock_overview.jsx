import React from 'react';
import StockCard from '../stock_card/stock_card.jsx';
import { Grid, Row, Col } from 'react-bootstrap'
import './stock_overview.sass';

export default class StockOverwiew extends React.Component {
    render() {
        return (
            <Row>
                <Col md={12}>
                    {(() => {
                        if (this.props.company) {
                            return <StockCard company={this.props.company} />
                        }
                    })()}
                </Col>
            </Row>
        );
    }
}