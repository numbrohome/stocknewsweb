# Stock News Website

It's an example of frontend app that uses separated api. Project was created with ReactJS, Webpack and it's tested with Mocha using "shallow rendering" approach.

### To install project

```sh
$ npm install
```
Please check "webpack.config.js" and change serverUrl to point to the right API (StockNewsApi)
```sh
    externals: {
        'Config': JSON.stringify(debug ? {
            // Development API
            serverUrl: "http://api.stocknews.local/"
        } : {
            // Production API
            serverUrl: "http://api.stocknews.local/"
        })
    },
```


### To start server

```sh
$ npm start
```

### To run tests

```sh
$ npm test
```